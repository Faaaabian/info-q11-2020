package de.gmi.rezeptverwaltung.model;

import de.gmi.rezeptverwaltung.drivers.Driver;
import de.gmi.rezeptverwaltung.interfaces.Observable;
import de.gmi.rezeptverwaltung.interfaces.Observer;
import de.gmi.rezeptverwaltung.interfaces.Viewdata;
import de.gmi.rezeptverwaltung.structures.Recipe;
import de.gmi.rezeptverwaltung.views.ChangeView;
import de.gmi.rezeptverwaltung.views.OverviewView;

public class OverviewModel implements Observable {

    public static final OverviewView overviewView = new OverviewView();
    private final Observer[] observers;
    private Viewdata state;
    private Driver driver;

    private OverviewModel() {
        observers = new Observer[1];
        //this.driver = Driver.getInstance();
    }

    public static OverviewView getInstance() {
        return overviewView;
    }

    public void create() {
        this.changeState(new ChangeView());
        this.notifyObservers();
    }

    public void changeState(Viewdata newState) {
        this.state = newState;
        notifyObservers();
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : observers) {
            observer.notify();
        }
    }

    @Override
    public Viewdata getState() {
        return this.state;
    }

    public void choose(Recipe recipe) {
        this.changeState(recipe);
        this.notifyObservers();
    }

    public Recipe[] next() {
        return null;
    }

    public Recipe[] last() {
        return null;
    }

    @Override
    public void addObserver(Observer observer) {
        observers[0] = observer; //ja ich weiß, das ist nicht dynamisch aber weil wir nur einen Observer haben ist es egal
    }

}
