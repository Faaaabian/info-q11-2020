package de.gmi.rezeptverwaltung.model;

import de.gmi.rezeptverwaltung.drivers.Driver;
import de.gmi.rezeptverwaltung.interfaces.Observable;
import de.gmi.rezeptverwaltung.interfaces.Observer;
import de.gmi.rezeptverwaltung.interfaces.Viewdata;
import de.gmi.rezeptverwaltung.structures.Recipe;

public class AddModel implements Observable {

    private static final AddModel addModel = new AddModel();
    private final Observer[] observers;
    private Viewdata state;
    private Driver driver;


    private AddModel() {
        observers = new Observer[1];
        //this.driver = Driver.getInstance();
    }

    public static AddModel getInstance() {
        return addModel;
    }

    public void save(String rezeptname, int zubereitungszeit, int portionen, int schwierigkeit, int kosten, boolean vegan){
        Driver.getInstance().save(rezeptname, zubereitungszeit, portionen, schwierigkeit, kosten, vegan);
    }

    public void back() {

    }

    public void changeState(Viewdata viewdata) {
        this.state = viewdata;
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : observers) {
            observer.notify();
        }
    }

    @Override
    public void addObserver(Observer observer) {
        observers[0] = observer;
    }

    @Override
    public Viewdata getState() {
        return this.state;
    }

    public void save(Recipe recipe) {
        Driver.getInstance().save(recipe);
    }
}
