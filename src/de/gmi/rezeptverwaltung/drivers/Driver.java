package de.gmi.rezeptverwaltung.drivers;

import de.gmi.rezeptverwaltung.list.List;
import de.gmi.rezeptverwaltung.structures.Recipe;

import javax.swing.*;
import java.sql.*;

public class Driver {
    private Connection con;
    private Statement stmt;
    private final String URL = "jdbc:mysql://localhost:3306/k73411_infosql?serverTimezone=Europe/Berlin";
    private final String USER = "root";
    private final String PASSWORD = "Ehrn38!lS";
    private final static Driver instance = new Driver();
    private int lastID = 0;


    private Driver() {
        this.startConnection();

    }

    public static Driver getInstance() {
        return instance;
    }

    private void startConnection() {
        try {
            this.con = DriverManager.getConnection(this.URL, this.USER, this.PASSWORD);
            this.stmt = con.createStatement();
            System.out.println("Verbunden");
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());

        }
    }

    public List search(String recipeToSearch) {//stellt nur das SQL-Statement her
        String basic = "SELECT * FROM reciepes WHERE RECIEPE_NAME LIKE";
        String statementToExecute = basic + "\"%" + recipeToSearch + "%\"";
        return doRequestGetList(statementToExecute);//macht die Datenbankabfrage und gibt eine Liste mit Rezepten drin zurück
    }

    public void save(String reciepename, String ingredients, String instructions, int type, int portions, int preparationtime, int cookingtime, int difficulty, float cost) {
        try {
            if (ingredients.length() < 1) {
                ingredients = "No Ingredients were given";
            }
            if (instructions.length() < 1) {
                instructions = "No Instructions were given";
            }
            String request = "insert into reciepes (RECIEPE_NAME, INGREDIENTS,INSTRUCTIONS,TYPE,PORTIONS,PREPARATION_TIME,COOKING_TIME,DIFFICULTY,COST) values(";
            String values = "\"" + reciepename + "\"" + "," + "\"" + ingredients + "\"" + "," + "\"" + instructions + "\"" + "," + type + "," + portions + "," + preparationtime + "," + cookingtime + "," + difficulty + "," + cost + ")";
            System.out.println(request + values);
            Integer row = this.stmt.executeUpdate(request + values);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public List doRequestGetList(String stmttoexecute) {//SQL-Statement wird mitgegeben, man bekommt Liste zurück
        List returnlist = new List();
        try {
            ResultSet rs = this.stmt.executeQuery(stmttoexecute);
            while (rs.next()) {
                Recipe reztoadd = new Recipe(rs.getString("RECIEPE_NAME"), rs.getInt("PREPARATION_TIME"), rs.getInt("COOKING_TIME"), rs.getInt("PORTIONS"), rs.getInt("DIFFICULTY"), rs.getInt("COST"), rs.getInt("TYPE"), rs.getString("INGREDIENTS"), rs.getString("INSTRUCTIONS"));
                returnlist.add(reztoadd);
            }
            rs.close();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
            JOptionPane.showMessageDialog(null, "Datenbankverbindung fehlgeschlagen, bitte erneut versuchen");
            this.startConnection();
        }
        return returnlist;
    }

    public void closeConnection() {
        try {
            this.stmt.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        finally {
            stmt = null;
        }
    }

    public void save(Recipe recipe) {
        this.save(
                recipe.getName(),
                recipe.getZubereitungszeit(),
                recipe.getKochzeit(),
                recipe.getPortionen(),
                recipe.getSchwierigkeit(),
                recipe.getKosten(),
                recipe.getType(),
                recipe.getZutaten(),
                recipe.getText()
        );
    }
}
