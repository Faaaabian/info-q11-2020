package de.gmi.rezeptverwaltung.drivers;

import de.gmi.rezeptverwaltung.model.AddModel;
import de.gmi.rezeptverwaltung.model.OverviewModel;
import de.gmi.rezeptverwaltung.structures.Recipe;

public class Controller {

    private static Controller instance = new Controller();

    public Controller(){

    }

    public void save(String rezeptname, int zubereitungszeit, int portionen, int schwierigkeit, int kosten, boolean vegan){
        AddModel.getInstance().save(rezeptname, zubereitungszeit, portionen, schwierigkeit,kosten, vegan);
    }

    public void save(Recipe recipe){
        AddModel.getInstance().save(recipe);
    }

    public void create(){
        OverviewModel.getInstance().create();
    }

    public void choose(Recipe recipe) {
        OverviewModel.getInstance().choose(recipe);
    }

    public void back(){
        AddModel.getInstance().back();
    }

    public Recipe[] next(){
        return OverviewModel.getInstance().next();
    }

    public Recipe[] last() {
        return OverviewModel.getInstance().last();
    }

    public void search(String Suchanfrage) {
        OverviewModel.getInstance().search(Suchanfrage);
    }

    public static Controller getInstance(){
        return instance;
    }



}
