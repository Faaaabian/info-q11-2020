package de.gmi.rezeptverwaltung.interfaces;

public interface Observable {
    Observer[] observers = new Observer[0];

    public void notifyObservers();

    public void addObserver(Observer observer);

    public Viewdata getState();
}
