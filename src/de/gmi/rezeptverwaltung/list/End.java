package de.gmi.rezeptverwaltung.list;

import de.gmi.rezeptverwaltung.interfaces.Data;
import de.gmi.rezeptverwaltung.structures.Element;
import de.gmi.rezeptverwaltung.structures.Recipe;

public class End extends Element {
    public End() {

    }

    @Override
    public Data giveData() {
        return null;
    }

    @Override
    public Element after() {
        return this;
    }

    @Override
    public Data before(Data data) {
        return null;
    }

    @Override
    public Element lookUp(Data data) {
        return null;
    }

    @Override
    public Element add(Data data) {
        return new Node(data, this);
    }

    @Override
    public void printAll() {
        System.out.println("End");
    }


}
