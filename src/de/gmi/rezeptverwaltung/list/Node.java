package de.gmi.rezeptverwaltung.list;

import de.gmi.rezeptverwaltung.interfaces.Data;
import de.gmi.rezeptverwaltung.structures.Element;
import de.gmi.rezeptverwaltung.structures.Recipe;

import java.sql.SQLOutput;

public class Node extends Element {

    private Element follower;
    private Data data;

    public Node(Data data, Element follower) {
        this.follower = follower;
        this.data = data;
    }

    @Override
    public Data giveData() {
        return this.data;
    }

    @Override
    public Element after() {
        return this.follower;
    }

    @Override
    public Data before(Data data) {
        if (this.data == data) {
            return null;
        } else {
            Data current = this.follower.before(data);
            return current == null ? this.data : current;
        }
    }

    @Override
    public Element lookUp(Data data) {
        return this.data.equals(data) ? this : this.follower.lookUp(data);
    }

    @Override
    public Element add(Data data) {
        this.follower = this.follower.add(data);
        return this;
    }

    @Override
    public void printAll() {
        System.out.println("Node");
        this.data.print();
        this.follower.printAll();
    }
}
