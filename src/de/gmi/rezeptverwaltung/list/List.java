package de.gmi.rezeptverwaltung.list;

import de.gmi.rezeptverwaltung.interfaces.Data;
import de.gmi.rezeptverwaltung.interfaces.Viewdata;
import de.gmi.rezeptverwaltung.structures.Element;

public class List implements Viewdata {
    private Element start;

    /**
     * Creates a new list.
     * This list has no fixed Length.
     * This list only accepts Data that implements the
     * de.gmi.rezeptverwaltung.interfaces.Data interface
     */
    public List() {
        this.start = new End();
    }

    /**
     * Returns the first element but does not remove it.
     * @return      Data    The first Element in the list.
     */
    public Data firstElement() {
        return this.start.giveData();
    }

    /**
     * Adds a element to the end of the list
     * @param data  Data    The Element that should be added
     */
    public void add(Data data) {
        this.start = this.start.add(data);
    }

    /**
     * Adds a element to the front of the list
     * @param data  Data    The Element that should be added
     */
    public void push(Data data) {
        this.start = new Node(data, this.start);
    }

    /**
     * Returns the first element and removes it from the List
     * @return  Data    First element
     */
    public Data pop(){
        Data rtn = this.start.giveData();
        this.start = this.start.after();
        return rtn;
    }

    /**
     * Prints the whole list out to debug
     */
    public void printAll(){
        System.out.println("Start of List");
        this.start.printAll();
    }

    /**
     * Do not use this! Because why would you?
     * Return a shallow copy of the Element that has the explicit Data-Element
     * @param data      what it looks for
     * @return          what it found
     */
    @Deprecated
    public Element lookUp(Data data) {
        return this.start.lookUp(data).clone();
    }
}
