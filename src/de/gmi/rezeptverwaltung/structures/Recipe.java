package de.gmi.rezeptverwaltung.structures;

import de.gmi.rezeptverwaltung.interfaces.Data;
import de.gmi.rezeptverwaltung.interfaces.Viewdata;

import java.util.HashMap;
import java.util.Map;

public class Recipe implements Data, Viewdata {

    private final Type type;
    private String name;
    private int zubereitungszeit;
    private int kochzeit;
    private int portionen;
    private int schwierigkeit;
    private int kosten;
    private String zutaten;
    private String text;

    // Primary constructor
    // Muss alles können und alle Werte zuordnen
    public Recipe(String name, int zubereitungszeit, int kochzeit, int portionen, int schwierigkeit, int kosten, Type type, String zutaten, String text) {
        this.name             = name;
        this.zubereitungszeit = zubereitungszeit;
        this.kochzeit         = kochzeit;
        this.portionen        = portionen;
        this.schwierigkeit    = schwierigkeit;
        this.kosten           = kosten;
        this.type             = type;
        this.zutaten          = zutaten;
        this.text             = text;
    }

    // Secondary constructor
    public Recipe(String name, int zubereitungszeit, int kochzeit, int portionen, int schwierigkeit, int kosten, int type, String zutaten, String text) {
        this(name, zubereitungszeit, kochzeit, portionen, schwierigkeit, kosten, Type.get(type), zutaten, text);
    }

    public Recipe(int test) {
        this("", test, test, test, test, test, Type.get(1), "", "");
    }

    public void print() {
        System.out.println("    " + this.kochzeit);
    }

    public String getName() {
        return this.name;
    }

    public int getZubereitungszeit() {
        return this.zubereitungszeit;
    }

    public int getKochzeit() {
        return this.kochzeit;
    }

    public int getPortionen() {
        return this.portionen;
    }

    public int getSchwierigkeit() {
        return this.schwierigkeit;
    }

    public int getKosten() {
        return this.kosten;
    }

    public String getType() {
        return this.type.toString();
    }

    public int getSerializedType() {
        return this.type.getID();
    }

    public String getZutaten() {
        return this.zutaten;
    }

    public String getText() {
        return this.text;
    }

    public enum Type {
        FLEISCH(1),
        VEGETARISCH(2),
        VEGAN(3),
        GLUTENFREI(4),
        HALALL(5),
        LAKTOSEFREI(6),
        FRUTARISCH(7),
        ICHESSEGARNICHTSMEHRDASISTAMBESTENFUERDIEUMWELT(8);

        // Reverse-lookup map for getting the Type by ID
        private static final Map<Integer, Type> lookup = new HashMap<>();

        // Um statische Variablen zu initialisieren
        static {
            for (Type t : Type.values()) {
                lookup.put(t.getID(), t);
            }
            System.out.println(lookup);
        }

        private final int id;

        Type(int id) {
            this.id = id;
        }

        int getID() {
            return id;
        }

        public static Type get(int id) {
            return lookup.get(id);
        }
    }
}
