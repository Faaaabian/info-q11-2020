package de.gmi.rezeptverwaltung.structures;

import de.gmi.rezeptverwaltung.interfaces.Data;

public abstract class Element implements Cloneable {
    public abstract Data giveData();

    public abstract Element after();

    public abstract Data before(Data data);

    public abstract Element lookUp(Data data);

    public abstract Element add(Data data);

    public abstract void printAll();

    public Element clone() {
        try {
            return (Element) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }
}
