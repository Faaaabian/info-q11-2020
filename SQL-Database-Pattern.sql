CREATE DATABASE IF NOT EXISTS k73411_infosql;
USE k73411_infosql;

DROP TABLE IF EXISTS reciepes;
CREATE TABLE reciepes (
	ID bigint(20) NOT NULL AUTO_INCREMENT,
	RECIEPE_NAME varchar(100) NOT NULL,
	INGREDIENTS varchar(8000) default "Error during init",
    INSTRUCTIONS varchar(8000) default "No instructions were given",
    TYPE int default 0,
    PORTIONS int default 0,
    PREPARATION_TIME int default 0,
    COOKING_TIME int default 0,
    DIFFICULTY smallint default -1,
    COST float default 0.0,
    PRIMARY KEY (ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table items
--

LOCK TABLES reciepes WRITE;
INSERT INTO reciepes (RECIEPE_NAME, INGREDIENTS, INSTRUCTIONS, TYPE, PORTIONS, PREPARATION_TIME, COOKING_TIME, DIFFICULTY, COST)
			VALUES ("Alles verfügbar", "Mit allem",  "Sollte klappen", 2, 3, 4, 5, 6, 7.7);
INSERT INTO reciepes (RECIEPE_NAME, INGREDIENTS, INSTRUCTIONS, TYPE, PORTIONS, PREPARATION_TIME, COOKING_TIME, DIFFICULTY, COST)
			VALUES ("Alles verfügbar aber anders", "Mit allem und anderen werten",  "Sollte auch klappen", 1, 6, 5, 4, 3, 2.1);
INSERT INTO reciepes (RECIEPE_NAME, INGREDIENTS, INSTRUCTIONS, TYPE, PORTIONS, PREPARATION_TIME, COOKING_TIME, DIFFICULTY, COST)
			VALUES ("Ist zwar alles da aber", "Hier ist ein falscher wert für den Typ angegeben! Aufgepasst!",  "Sollte nicht klappen", 1332, 6, 5, 4, 3, 2.1);
INSERT INTO reciepes (RECIEPE_NAME)
			VALUES ("Es gib nur nen namen");
INSERT INTO reciepes (RECIEPE_NAME, COST)
			VALUES ("Name und Kosten", 7.47);
INSERT INTO reciepes (RECIEPE_NAME, PORTIONS)
			VALUES ("Portionen und Name", 4);
            
UNLOCK TABLES;